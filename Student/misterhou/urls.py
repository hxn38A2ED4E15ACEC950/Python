from django.urls import path
from . import views
urlpatterns = [
path('',views.home,name="home"),

path('user',views.user,name="user"),

path('enroll',views.enroll,name="enroll"),

path('test',views.test,name="test"),

path('check',views.check,name="check"),

path('manager',views.manager,name="manager"),

path('background',views.background,name="background"),

path('Stu',views.Stu,name="Stu"),

path('Cour',views.Cour,name="Cour"),

path('Stu_add',views.Stu_add,name="Stu_add"),

path('Stu_add1',views.Stu_add1,name="Stu_add1"),

path('Cour_add',views.Cour_add,name="Cour_add"),

path('Cour_add1',views.Cour_add1,name="Cour_add1"),

path('Stu_alt',views.Stu_alt,name="Stu_alt"),

path('Stu_alt1',views.Stu_alt1,name="Stu_alt1"),

path('Stu_alt2',views.Stu_alt2,name="Stu_alt2"),

path('Cour_alt',views.Cour_alt,name="Cour_alt"),

path('Cour_alt1',views.Cour_alt1,name="Cour_alt1"),

path('Cour_alt2',views.Cour_alt2,name="Cour_alt2"),

path('Stu_del',views.Stu_del,name="Stu_del"),

path('Stu_del1',views.Stu_del1,name="Stu_del1"),

path('Cour_del',views.Cour_del,name="Cour_del"),

path('Cour_del1',views.Cour_del1,name="Cour_del1"),

path('per',views.per,name="per"),

path('per_alt1',views.per_alt1,name="per_alt1"),

path('per_alt2',views.per_alt2,name="per_alt2"),

path('grade',views.grade,name="grade"),

path('grade_add',views.grade_add,name="grade_add"),

path('grade_add1',views.grade_add1,name="grade_add1"),

path('grade_alt',views.grade_alt,name="grade_alt"),

path('grade_alt1',views.grade_alt1,name="grade_alt1"),

path('grade_alt2',views.grade_alt2,name="grade_alt2"),

path('grade_del',views.grade_del,name="grade_del"),

path('grade_del1',views.grade_del1,name="grade_del1"),

path('percour',views.percour,name="percour"),

path('pergrade',views.pergrade,name="pergrade"),


]