from django.shortcuts import render,redirect
from django.http import HttpResponse
from misterhou.models import User2,Student,Course,SC
from misterhou import models
from django.contrib import auth

# Create your views here.

def home(request):
    return render(request,'login.html',{})

def check(request):
    return render(request,'Work1-1.html',{})

def per(request):
        # context=request.session.get('msg')
        # print(context)
        name = request.session["name"]
        student_list = Student.objects.filter(Sid=name)
        #print(student_list.Sid)

        return render(request,'per.html',{'student_list':student_list})

def per_alt1(request):
        name = request.session["name"]
        user = Student.objects.filter(Sid=name)
        print(user)
        return render(request,'Stu_alt1.html',{'user':user})

def per_alt2(request):
        user_name = request.POST['user_name']
        name = request.POST['name']
        age = request.POST['age']
        user_sex = request.POST['user_sex']
        user_dept = request.POST['user_dept']
        user_phone1 = request.POST['user_phone1']

        Student.objects.filter(Sid=user_name).update(Sname=name,Sage=age,Ssex=user_sex,Sdept=user_dept,Stel=user_phone1)

        student_list=models.Student.objects.all()

        return render(request,'per_alt2.html',{'student_list':student_list})


def manager(request):
    return render(request,'manager.html',{})

def background(request):
    return render(request,'background.html',{})

def Stu(request):
        Students = Student.objects.all()
        print(Students)
        return render(request,'Stu.html',{'Students':Students})

def Cour(request):
        Courses = Course.objects.all()
        print(Courses)
        return render(request,'Courses.html',{'Courses':Courses})

        
def Stu_add(request):
        return render(request,'Stu_add.html',{})

def Stu_add1(request):
        user_name = request.POST['user_name']
        name = request.POST['name']
        age = request.POST['age']
        user_sex = request.POST['user_sex']
        user_dept = request.POST['user_dept']
        user_phone1 = request.POST['user_phone1']

        Student.objects.create(Sid=user_name,Sname=name,Sage=age,Ssex=user_sex,Sdept=user_dept,Stel=user_phone1)

        student_list=models.Student.objects.all()

        return render(request,'Stu_add1.html',{'student_list':student_list})

def Cour_add(request):
        return render(request,'Cour_add.html',{})

def Cour_add1(request):
        Cid = request.POST['Cid']
        Cname = request.POST['Cname']
        Ccredit = request.POST['Ccredit']

        Course.objects.create(Cid=Cid,Cname=Cname,Ccredit=Ccredit)

        course_list=models.Course.objects.all()

        return render(request,'Cour_add1.html',{'course_list':course_list})

def Stu_alt(request):
        return render(request,'Stu_alt.html',{})

def Stu_alt1(request):
        user_name = request.POST['user_name']
        user = Student.objects.filter(Sid=user_name)
        print(user)
        return render(request,'Stu_alt1.html',{'user':user})

def Stu_alt2(request):
        user_name = request.POST['user_name']
        name = request.POST['name']
        age = request.POST['age']
        user_sex = request.POST['user_sex']
        user_dept = request.POST['user_dept']
        user_phone1 = request.POST['user_phone1']

        Student.objects.filter(Sid=user_name).update(Sname=name,Sage=age,Ssex=user_sex,Sdept=user_dept,Stel=user_phone1)

        student_list=models.Student.objects.all()

        return render(request,'Stu_alt2.html',{'student_list':student_list})


def Cour_alt(request):
        return render(request,'Cour_alt.html',{})

def Cour_alt1(request):
        cid = request.POST['user_name']
        user = Course.objects.filter(Cid=cid)
        print(user)
        return render(request,'Cour_alt1.html',{'user':user})

def Cour_alt2(request):
        Cid = request.POST['C_id']
        Cname = request.POST['Cname']
        credit = request.POST['credit']
        

        Course.objects.filter(Cid=Cid).update(Cname=Cname,Ccredit=credit)

        course_list=models.Course.objects.all()

        return render(request,'Cour_alt2.html',{'course_list':course_list})

def Stu_del(request):
        return render(request,'Stu_del.html',{})

def Stu_del1(request):
        user_name = request.POST['user_name']
        Student.objects.filter(Sid=user_name).delete()

        Students = Student.objects.all()
        print(Students)
        return render(request,'Stu.html',{'Students':Students})

def Cour_del(request):
        return render(request,'Cour_del.html',{})

def Cour_del1(request):
        cid = request.POST['c_id']
        Course.objects.filter(Cid=cid).delete()

        Courses = Course.objects.all()
        print(Courses)
        return render(request,'Stu.html',{'Courses':Courses})

def grade(request):
        grades = SC.objects.all()
        print(grades)
        return render(request,'grade.html',{'grades':grades})

def grade_add(request):
        return render(request,'grade_add.html',{})

def grade_add1(request):
        user_name = request.POST['user_name']
        cid = request.POST['cid']
        grade = request.POST['grade']

        SC.objects.create(Sid=user_name,Cid=cid,grade=grade)

        grades=models.SC.objects.all()

        return render(request,'grade_add1.html',{'grades':grades})

def grade_alt(request):
        return render(request,'grade_alt.html',{})

def grade_alt1(request):
        user_name = request.POST['user_name']
        grade = SC.objects.filter(Sid=user_name)
        print(grade)
        return render(request,'grade_alt1.html',{'grade':grade})

def grade_alt2(request):
        user_name = request.POST['user_name']
        Cid = request.POST['cid']
        grade = request.POST['grade']

        SC.objects.filter(Sid=user_name).update(Cid=Cid,grade=grade)

        grades=models.SC.objects.all()

        return render(request,'grade_alt2.html',{'grades':grades})

def grade_del(request):
        return render(request,'grade_del.html',{})

def grade_del1(request):
        user_name = request.POST['user_name']
        SC.objects.filter(Sid=user_name).delete()

        grades = SC.objects.all()
        print(grades)
        return render(request,'grade.html',{'grades':grades})

def percour(request):
        name = request.session["name"]
        grade = SC.objects.filter(Sid=name)

        for grades in grade:
                cid = grades.Cid
        print(cid)
        grades = Course.objects.filter(Cid=cid)

        return render(request,'percour.html',{'grade':grade,'grades':grades})

def pergrade(request):
        name = request.session["name"]
        grade = SC.objects.filter(Sid=name)

        return render(request,'pergrade.html',{'grade':grade})

def user(request):
    try:
        Userid = request.POST['userid']
        Password = request.POST['password']
        request.session["name"] = Userid
        # context={}
        # context['userid']=userid
        # request.session['msg']=context

        print(Userid)
        print(Password)

        user = User2.objects.filter(userid=Userid,password=Password)
        print(user)
        if user:
                for users in user:
                        if users.status=='M':
                                return render(request,'manager.html',{})
                        if users.status=='S':
                                return render(request,'Student.html',{'user':user})

        else:
                return render(request,'Work1-1.html',{'msg':'用户名或密码不正确'})
    except:
         return render(request,'login.html',{'msg':None})


def enroll(request):
    return render(request,'enroll.html',{})

def test(request):
    user_name = request.POST['user_name']
    user_password = request.POST['user_password']
    status = request.POST['status']
    #user_name=request.POST.get("userid",None)
    #user_password=request.POST.get("password",None)

    from misterhou.models import User2
    from misterhou import models

    User2.objects.create(userid=user_name,password=user_password,status=status)

    info_list=models.User2.objects.all()

    return render(request,'login.html',{"info_list":info_list})



